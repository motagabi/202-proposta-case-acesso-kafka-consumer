package br.com.mastertech.consumer;

import br.com.mastertech.producer.Acesso;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class AcessoConsumer {

    @KafkaListener(topics = "spec4-gabriela-mota-1", groupId = "motagabi")
    public void receber(@Payload Acesso acesso) {
        System.out.println("Recebi o Log " + acesso.getId() + " que foi gerado pela chave: ClienteId " + acesso.getClienteId() + " PortaId " + acesso.getPortaId());
    }

}
